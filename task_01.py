def phrase_search(object_list: list, search_string: str) -> int:
    
    def skip_str(generator, symbol):
        '''
        Функция принимает итератор и символ, который мы будем искать
        Задача: прогнать интератор до нужного символа 
        '''
        c = None
        while c != symbol:
            c = next(generator)

    def my_iterator(string):
        '''
        Функция-итератор, в которую передается строка
        С каждой итерацией возвращает следующий символ в лоу-кейсе
        '''
        for char in string:
            yield char.lower()

    def check_literal(str2, slots):
        '''
        Функция проверки литерала {} в строке-шаблоне на совпадение
        с одной из строк в slots
        str2 приходит в обрезанном виде
        str2 - название переменной дурацкое 
        '''
        for slot in slots:
            if str2.lower().startswith(slot.lower()):
                # Если str2 начинается с строки slot,
                # значит литерал разобран правильно
                # и совпадение есть, возвращаем tuple
                # (успешность операции, длина совпавшего слова)
                return (True, len(slot))
        return (False, -1)

    for list_item in object_list:

        phrase = list_item.get('phrase')

        # Создаем да конечных автомата через функцию-итератор
        # Фраза-шаблон из объекта
        gen_phrase = my_iterator(phrase)
        # и строка поиска
        gen_user = my_iterator(search_string)

        # собираем список слотов, которые могут встречаться 
        # на месте литералов
        my_slots = list_item.get('slots')
        # дополняем наш список всеми зачениями внутри {} строки-шаблона
        my_slots += [item.split('}')[0] for item in phrase.split('{') if len(item.split('}')) > 1]

        # Положение считывающей головки
        i = 0
        word_parsed = True
        while True:
            try:
                # берем по символу из обеих строк
                char_phrase = next(gen_phrase)
                char_user = next(gen_user)
            except StopIteration:
                # Если мы брейкнулись здесь и нас никто не прогнал раньше,
                # значит одна или обе строки закончились, при этом 
                # проблем с парсингом символов не было
                break

            if char_phrase == '{':
                # Если наткнулись на литерал в строке-шаблоне, то
                # отрезаем пройденный кусок от строки поиска
                # и вместе со списком слов отправляем их парситься

                # Пример:
                # search_string = "Привет, Евгений"
                # my_slots = ["Евгений", "Петрович"]
                # phrase = "Привет, {name}"
                # В функцию улетит кусок строки - "Евгений!" и ["Евгений", "Петрович"]
                success, length = check_literal(search_string[i:], my_slots)

                if not success:
                    word_parsed = False
                    break	
                # если разбор литерала прошел успешно,
                # и это не конец строки, то переопределяем итератор
                # заново с новой строкой (отрезанной) и двигаем 
                # считывающую переменную на количество позиций слота
                # параллельно строку-шаблон сдвигаем к закрывающейся скобке
                elif search_string[i+length:] != '':
                    gen_user = my_iterator(search_string[i+length:])	
                    i = i + length
                    skip_str(gen_phrase, "}")		
                    continue
                else:
                    break			

            # Если очередные символы не совпадают
            if char_phrase != char_user:
                word_parsed = False
                break
            
            i += 1

        if word_parsed:
            return list_item.get('id')
    return 0


if __name__ == "__main__":
    """ 
    len(object) != 0
    object["id"] > 0
    0 <= len(object["phrase"]) <= 120
    0 <= len(object["slots"]) <= 50
    """
    object = [
        {"id": 1, "phrase": "Hello world!", "slots": []},
        {"id": 2, "phrase": "I wanna {pizza}", "slots": ["pizza", "BBQ", "pasta"]},
        {"id": 3, "phrase": "Give me your power", "slots": ["money", "gun"]},
    ]

    assert phrase_search(object, 'I wanna pasta') == 2
    assert phrase_search(object, 'Give me your power') == 3
    assert phrase_search(object, 'Hello world!') == 1
    assert phrase_search(object, 'I wanna nothing') == 0
    assert phrase_search(object, 'Hello again world!') == 0
    assert phrase_search(object, 'I need your clothes, your boots & your motorcycle') == 0